﻿using BowlingScore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBowlingGame
{
    class Program
    {
        static void Main(string[] args)
        {
            BowlingGame bg = new BowlingGame();
            Frame[] frames = new Frame[10];

            bg.Start();

            int input = Convert.ToInt32(Console.ReadLine());

            //Start a FOR loop to play available number of frames
            bool bStrike = false;
            bool bSpare = false;
            for(int i = 0; i < frames.Length; i++)
            {
                bg.Roll(input);
                bStrike = bg.GetCurrentFrame().GetbStrike();

                //Handle strike and bonuses
                if (bStrike)
                {
                    frames[i] = new Frame(bg.GetCurrentFrame());
                    Console.WriteLine("It's a strike!");
                }
                //If no strike, then roll again
                Console.Write("Enter next number of pins dropped: ");
                bg.Roll(Convert.ToInt32(Console.ReadLine()));
                bSpare = bg.GetCurrentFrame().GetbSpare();

                if(bSpare)
                {
                    //
                }

                bg.EndRound();
            }

            bg.DisplayBoard(frames);

            ConsoleKeyInfo pause = Console.ReadKey();
        }
    }
}
