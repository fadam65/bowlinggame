﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingScore
{
    public class Frame
    {
        private int RollOne;
        private int RollTwo;
        private int Score;
        private bool IsSpare;
        private bool IsStrike;
        private bool IsGutterBall;
        private bool LastFrame;
        private bool AwaitingBonus;

        public Frame()
        {
            RollOne = 0;
            RollTwo = 0;
            Score = 0;
            IsSpare = false;
            IsStrike = false;
            IsGutterBall = false;
            LastFrame = false;
        }
        //Copy constructors
        public Frame(Frame orig)
        {
            SetRollOne(orig.GetRollOne());
            SetRollTwo(orig.GetRollTwo());
            SetScore(orig.GetScore());
            SetSpare(orig.GetbSpare());
            SetStrike(orig.GetbStrike());
            SetGutter(orig.GetbGutter());
            SetAwaitingBonus(orig.GetAwaitingBonus());
        }

        public void SetRollOne(int roll) { RollOne = roll; }
        public int GetRollOne() { return RollOne; }

        public void SetRollTwo(int roll) { RollTwo = roll; }
        public int GetRollTwo() { return RollTwo; }

        private void SetScore(int s) { Score = s; }
        public int GetScore() { return Score; }

        public void SetSpare(bool status) { IsSpare = status; }
        public bool GetbSpare () { return IsSpare; }

        public void SetStrike(bool status) { IsStrike = status; }
        public bool GetbStrike() { return IsStrike; }

        public void SetGutter(bool status) { IsGutterBall = status; }
        public bool GetbGutter() { return IsGutterBall; }

        public void SetAwaitingBonus(bool status) { AwaitingBonus = status; }
        public bool GetAwaitingBonus() { return AwaitingBonus; }

        public void AddToScore(int s)
        {
            Score += s;
        }

        public void DisplayFrame()
        {
            Console.WriteLine("First Roll: " + GetRollOne() + " | Second Roll: " + GetRollTwo() +
                "| Strike: " + GetbStrike() + "| Spare: " + GetbSpare());
        }

        public void ClearFrame()
        {
            SetRollOne(0);
            SetRollTwo(0);
            SetScore(0);
            IsSpare = false;
            IsStrike = false;
            IsGutterBall = false;
        }

    }
}
