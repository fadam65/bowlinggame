﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingScore
{
    public class BowlingGame : IGame
    {
        private int score;
        public int Score //read-write instance property
        {
            get { return score; }
            set { score = value; }
        }

        private Frame CurrentFrame;

        public BowlingGame()
        {
            CurrentFrame = new Frame();
            Score = 0;
        }

        public Frame GetCurrentFrame() { return CurrentFrame; }

        public void Start()
        {
            Console.WriteLine("Playing a game of Bowling: ");
            Console.Write("Please enter the number of pins dropped: ");
        }

        public void Roll(int s)
        {
            //Determining if rolling first try
            if ((CurrentFrame.GetRollOne() == 0) && !(CurrentFrame.GetbGutter()))
            {
                if (s == 10) //If a strike
                {
                    CurrentFrame.SetStrike(true);
                    CurrentFrame.SetAwaitingBonus(true);
                    //Score for individual frame
                    CurrentFrame.AddToScore(s);
                }
                else if (s == 0) // If gutterball, RollOne is already a 0
                { CurrentFrame.SetGutter(true); }
                else
                {
                    CurrentFrame.SetRollOne(s);
                    CurrentFrame.AddToScore(s);
                }
            } else //Rolling second try
            {
                int total = CurrentFrame.GetRollOne() + s; 
                if( total == 10 )
                {
                    CurrentFrame.SetSpare(true);
                    CurrentFrame.AddToScore(s);
                }
                else
                {
                    CurrentFrame.SetRollTwo(s);
                    CurrentFrame.AddToScore(s);
                }
            }
        }

        public void EndRound()
        {
            CurrentFrame.ClearFrame();
        }

        public int CountFrames(Frame[] frames)
        {
            for (int i = 0; i < frames.Length; i++)
            {
                Score += frames[i].GetScore();
            }
            return Score;
        }

        public void DisplayBoard(Frame[] frames)
        {
            for (int i = 0; i < frames.Length; i++)
            {
                Console.Write(i + 1 + ')'); 
                frames[i].DisplayFrame();
            }
        }
    }
}
