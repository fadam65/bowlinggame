﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingScore
{
    interface IGame
    {
        int Score { get; set; }

        void Start();
        void Roll(int s);
    }
}
