﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BowlingScore;

namespace BowlingGameTests
{
    [TestClass]
    public class FrameScoreTests
    {
   
        [TestMethod]
        public void Single_Frame_OpenFrame_Test()
        {
            //Arrange: initializing objects and setting values that are needed
            BowlingGame bg = new BowlingGame();
            Frame fr = new Frame();

            //Act: invoking the methods on the objects
            bg.Roll(5);
            bg.Roll(3);
            fr = bg.GetCurrentFrame();
            int score = fr.GetScore();

            //Assert: check to see that values we got back are what we were expecting
            Assert.AreEqual(8, score);
            Assert.IsFalse(fr.GetbStrike());
            Assert.IsFalse(fr.GetbSpare());
        }

        [TestMethod]
        public void Single_Frame_Strike_Test()
        {
            //Arrange: initializing objects and setting values that are needed
            BowlingGame bg = new BowlingGame();
            Frame fr = new Frame();

            //Act: invoking the methods on the objects
            bg.Roll(10);
            fr = bg.GetCurrentFrame();
            int score = fr.GetScore();

            //Assert: check to see that values we got back are what we were expecting
            Assert.AreEqual(10, score);
            Assert.IsTrue(fr.GetbStrike());
            Assert.IsFalse(fr.GetbSpare());
        }

        [TestMethod]
        public void Single_Frame_Spare_Test()
        {
            //Arrange: initializing objects and setting values that are needed
            BowlingGame bg = new BowlingGame();
            Frame fr = new Frame();

            //Act: invoking the methods on the objects
            bg.Roll(6);
            bg.Roll(4);
            fr = bg.GetCurrentFrame();
            int score = fr.GetScore();

            //Assert: check to see that values we got back are what we were expecting
            Assert.AreEqual(10, score);
            Assert.IsFalse(fr.GetbStrike());
            Assert.IsTrue(fr.GetbSpare());
        }

        [TestMethod]
        public void Single_Frame_Gutterball_Test()
        {
            //Arrange: initializing objects and setting values that are needed
            BowlingGame bg = new BowlingGame();
            Frame fr = new Frame();

            //Act: invoking the methods on the objects
            bg.Roll(0);
            bg.Roll(0);
            fr = bg.GetCurrentFrame();
            int score = fr.GetScore();

            //Assert: check to see that values we got back are what we were expecting
            Assert.AreEqual(0, score);
            Assert.IsFalse(fr.GetbStrike());
            Assert.IsFalse(fr.GetbSpare());
            Assert.IsTrue(fr.GetbGutter());

        }

        [TestMethod]
        public void All_Frames_OpenFrame_Test()
        {
            //Arrange: initializing objects and setting values that are needed
            BowlingGame bg = new BowlingGame();
            Frame[] frames = new Frame[10];

            //Act: invoking the methods on the objects
            for (int i = 0; i < frames.Length; i++)
            {
                bg.Roll(4);
                bg.Roll(4);
                frames[i] = new Frame(bg.GetCurrentFrame());
                bg.EndRound();
            }
            int score = bg.CountFrames(frames);

            //Assert: check to see that values we got back are what we were expecting
           Assert.AreEqual(80, score);
        }

        [TestMethod]
        public void Perfect_Game_Test()
        {
            //Arrange: initializing objects and setting values that are needed
            BowlingGame bg = new BowlingGame();
            Frame[] frames = new Frame[10];

            //Act: invoking the methods on the objects
            for (int i = 0; i < frames.Length; i++)
            {
                bg.Roll(10);
                frames[i] = new Frame(bg.GetCurrentFrame());
                if (i != 0 && frames[i - 1].GetbStrike())
                {
                    frames[i - 1].AddToScore(frames[i].GetScore());
                }
                if (i >= 2 && frames[i - 2].GetAwaitingBonus())
                {
                    frames[i - 2].AddToScore(frames[i].GetScore());
                    frames[i - 2].SetAwaitingBonus(false);
                }
                if(i == (frames.Length - 1) && frames[i].GetbStrike())
                {
                    bg.Roll(10);
                    bg.Roll(10);
                    frames[i].AddToScore(bg.GetCurrentFrame().GetScore());
                }
                bg.EndRound();
            }
            int score = bg.CountFrames(frames);

            //Assert: check to see that values we got back are what we were expecting
            Assert.AreEqual(300, score);
        }
    }
}
